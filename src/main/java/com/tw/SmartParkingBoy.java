package com.tw;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        int maxCapacity = 0;
        int indexOfMaxCapacity = 0;
        for (ParkingLot parkingLot : super.getParkingLots()) {
            if (parkingLot.getCapacity() > maxCapacity) {
                maxCapacity = parkingLot.getCapacity();
                indexOfMaxCapacity = super.getParkingLots().indexOf(parkingLot);
            }
        }
        return super.getParkingLots().get(indexOfMaxCapacity).park(car);
    }
    // --end->
}
