package com.tw;

public class SuperSmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        double maxEmptyRate = 0;
        int indexOfMaxEmptyRate = 0;
        double emptyRate = 0;
        for (ParkingLot parkingLot : super.getParkingLots()) {
            emptyRate = parkingLot.getAvailableParkingPosition()/parkingLot.getCapacity();
            if (emptyRate > maxEmptyRate) {
                maxEmptyRate = emptyRate;
                indexOfMaxEmptyRate = super.getParkingLots().indexOf(parkingLot);
            }
        }
        return super.getParkingLots().get(indexOfMaxEmptyRate).park(car);
    }
    // --end->
}
