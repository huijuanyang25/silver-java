package com.tw;

public class NormalParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        for (int i = 0; i < super.getParkingLots().size(); i++) {
            try {
                return super.getParkingLots().get(i).park(car);
            } catch (ParkingLotFullException e) {
                i++;
            }
        }
        super.setLastErrorMessage("The parking lot is full.");
        return null;
    }
    // --end->
}
