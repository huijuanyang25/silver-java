package com.tw;

import java.util.*;

public abstract class ParkingBoyBase implements ParkingBoy {
    private final List<ParkingLot> parkingLots = new ArrayList<>();
    private String lastErrorMessage;

    @Override
    public void addParkingLot(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    // TODO: You can override methods or add new methods here if you want
    // <-start-
    public Car fetch(ParkingTicket ticket) {
        if (ticket == null) {
            this.setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }

        for (int i = 0; i < this.getParkingLots().size(); i++) {
            try {
                return this.getParkingLots().get(i).fetch(ticket);
            } catch (InvalidParkingTicketException e) {
                i++;
            }
        }
        this.setLastErrorMessage("Unrecognized parking ticket.");
        return null;
    }
    // --end->

    @Override
    public String getLastErrorMessage() {
        return lastErrorMessage;
    }

    protected void setLastErrorMessage(String errorMessage) {
        lastErrorMessage = errorMessage;
    }

    protected List<ParkingLot> getParkingLots() {
        return Collections.unmodifiableList(this.parkingLots);
    }
}
